import { useEffect, useState } from "react";
import { SimpleTable } from "../../../components/FacilitySummarySection/SimpleTable";
import { LatestSpaces } from "./LatestSpaces";
import {
  IFetchedSpace,
  getAllSpacesForFacility,
} from "../../../services/SpaceServices";
import { useParams } from "react-router-dom";
import { LoadingScreen } from "../../LoadingScreen/LoadingScreen";

export const FMDashboard = () => {
  const { facilityId } = useParams();
  const [spaces, setSpaces] = useState<IFetchedSpace[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    getAllSpacesForFacility(Number(facilityId)).then((f) => {
      if (f) {
        setSpaces(f);
      }
    });
    setIsLoading(false);
  }, [facilityId]);

  if (isLoading) {
    return <LoadingScreen />;
  } else
    return (
      <>
        <LatestSpaces spaces={spaces.slice(0, 3)} />
        <SimpleTable name="punches" limit={5} />
        <SimpleTable name="assets" limit={5} />
      </>
    );
};
