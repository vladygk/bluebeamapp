import { FC, useState } from "react";
import { SpaceContext } from "./ISpaceContext";
import { IPoint } from "../components/Drawing/IPoint";

export const SpaceProvider: FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [isActiveForm, setIsActiveForm] = useState<boolean>(false);
  const [buttonType, setButtonType] = useState<string>();
  const [isDrawable, setIsDrawable] = useState<boolean>(false);
  const [clickedPointData, setClickedPointData] = useState<IPoint>({
    id: 70000,
    title: "",
    description: "",
    assigneeId: null,
    assignee: null, // Name
    assetId: null,
    asset: "",
    expirationDate: "",
    startDate: "",
    spaceId: "",
    creatorId: "1",
    status: "Unassinged",
    coordX: -1,
    coordY: -1,
    statusId: -1,
    isClicked: false,
  });
  const [points, setPoints] = useState<IPoint[]>([]);
  const [activeButton, setActiveButton] = useState<string>("space");

  return (
    <>
      <SpaceContext.Provider
        value={{
          isActiveForm: isActiveForm,
          setIsActiveForm: setIsActiveForm,
          buttonType: buttonType!,
          setButtonType: setButtonType,
          isDrawable: isDrawable,
          setIsDrawable: setIsDrawable,
          points: points,
          setPoints: setPoints,
          clickedPointData: clickedPointData!,
          setClickedPointData: setClickedPointData,
          activeButton: activeButton,
          setActiveButton: setActiveButton,
        }}
      >
        {children}
      </SpaceContext.Provider>
    </>
  );
};
