import { useContext } from "react";
import photo from "/src/assets/photos/white.png";
import { ThemeContext } from "../../contexts/ThemeContext";

export const PlaceholderImage = () => {
  const { theme } = useContext(ThemeContext);

  return (
    <img
      className={`border-2 ${
        theme == "darkTheme" && "dark-img"
      } rounded-md border-[gray] w-[100%] h-[350px] lg:h-[250px]`}
      src={photo}
      alt="Placeholder"
    />
  );
};
