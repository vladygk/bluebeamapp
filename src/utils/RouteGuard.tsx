import { useContext, FC, useState } from "react";
import { AuthContext } from "../contexts";

interface IRouteGuard {
  Component: FC<any>;
  DefaultComponent: FC<any>;
}

export const RouteGuard: FC<IRouteGuard> = ({
  Component,
  DefaultComponent,
}) => {
  const { userData } = useContext(AuthContext);

  const [, setGuarded] = useState(false);

  if (userData) {
    return <Component />;
  } else {
    history.replaceState("", "", "/login");

    setGuarded(true);
    return <DefaultComponent />;
  }
};
