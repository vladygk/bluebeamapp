import daisyui from "daisyui";

/** @type {import('tailwindcss').Config} */
export default {
  daisyui: {
    themes: [
      {
        darkTheme: {
          primary: "#121212",

          secondary: "#BB86FC",

          accent: "#BB86FC",

          neutral: "#787878",

          "base-100": "#ffffff",

          info: "#752DD4",

          success: "#BB86FC",

          warning: "#91F521",

          error: "#F87272",

          background: "#000000",

          text: "#ffffff",
        },
      },
      {
        lightTheme: {
          primary: "#ffffff",

          secondary: "#377CFB",

          accent: "#000000",

          neutral: "#333C4D",

          "base-100": "#FFFFFF",

          info: "#CFD7E6",

          success: "#E9EBEE",

          warning: "#CFD7E6",

          error: "#F87272",
          background: "#ffffff",
        },
      },
    ],
  },
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    screens: {
      "2xl": { max: "1535px" },

      xl: { max: "1279px" },

      lg: { max: "1024px" },

      md: { max: "912px" },

      sm: { max: "480px" },
    },
  },
  plugins: [daisyui],
};
