export interface IPoint {
  id: number;
  coordX: number;
  coordY: number;
  status: string;
  statusId: number;
  size?: IPointSize;
  title: string;
  assigneeId?: number | null;
  assignee?: string | null;
  expirationDate: string;
  startDate: string;
  description: string;
  assetId?: number | null;
  asset: string;
  creatorId: string;
  spaceId: string;
  isClicked: boolean;
}

interface IPointSize {
  width: string;
  height: string;
}
