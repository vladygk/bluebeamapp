import { Box } from "@mantine/core";
import { MRT_ColumnDef, MantineReactTable } from "mantine-react-table";
import { useCallback, useContext, useEffect, useMemo, useState } from "react";
import {
  IFetchedUser,
  getAllUsersFromFacility,
  removeUserFromFacility,
} from "../../services/auth";
import { useParams } from "react-router-dom";
import { AuthContext, ThemeContext } from "../../contexts";
import { LoadingScreen } from "../../pages/LoadingScreen/LoadingScreen";

export const UsersTable = () => {
  const [users, setUsers] = useState<IFetchedUser[]>([]);
  const [isSmallScreen] = useState(false);
  const { userRole } = useContext(AuthContext);
  const { toggleMantineTable, theme } = useContext(ThemeContext);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const { facilityId } = useParams();

  useEffect(() => {
    getAllUsersFromFacility(Number(facilityId)).then((f) => {
      if (f !== undefined) {
        setUsers(f.reverse());
      }
    });
    setIsLoading(false);
  }, [facilityId]);

  useEffect(() => {
    toggleMantineTable();
  }, [theme, toggleMantineTable, users]);

  const handleUserDelete = useCallback(
    async (id: number) => {
      await removeUserFromFacility(id, Number(facilityId));
      if (users === null) {
        return;
      }
      setUsers([
        ...users.filter((user: IFetchedUser) => Number(user.id) !== id),
      ]);
    },
    [facilityId, users]
  );

  const columns = useMemo<MRT_ColumnDef<IFetchedUser>[]>(
    () => [
      {
        id: "Name", //id used to define `group` column
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.name}`, //accessorFn used to join multiple data into a single cell
            id: "Name", //id is still required when using accessorFn instead of accessorKey
            header: "Name",
            size: isSmallScreen ? 40 : 200,
            Cell: ({ renderedCellValue }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <span>{renderedCellValue}</span>
              </Box>
            ),
          },
        ],
      },
      {
        id: "userInfo",
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.email}`,
            header: "Email",
            size: isSmallScreen ? 40 : 200,
            //custom conditional format and styling
            Cell: ({ cell }) => (
              <Box>
                <span>{cell.getValue<string>()}</span>
              </Box>
            ),
          },
          {
            accessorFn: (row) => `${row.role}`,
            accessorKey: "role",
            header: "Role",
            size: isSmallScreen ? 40 : 200,
            //custom conditional format and styling
            Cell: ({ cell }) => (
              <Box
                sx={(theme) => ({
                  backgroundColor:
                    cell.getValue<string>() == "Owner"
                      ? theme.colors.yellow[8]
                      : theme.colors.blue[8],
                  borderRadius: "4px",
                  color: "#fff",
                  maxWidth: "11ch",
                  padding: "4px",
                  textAlign: "center",
                })}
              >
                <span>{cell.getValue<string>()}</span>
              </Box>
            ),
          },
          {
            accessorFn: (row) => `${row.id}`,
            header: "Remove from facility",
            size: isSmallScreen ? 40 : 200,
            enableColumnFilter: false,
            enableSorting: false,

            Cell: ({ cell }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <button
                  className="btn btn-neutral py-0"
                  onClick={() =>
                    handleUserDelete(Number(cell.getValue<number>()))
                  }
                  disabled={userRole !== "FM"}
                >
                  Delete
                </button>
              </Box>
            ),
          },
        ],
      },
    ],
    [isSmallScreen, userRole, handleUserDelete]
  );

  if (isLoading) {
    return <LoadingScreen />;
  } else
    return (
      <MantineReactTable
        columns={columns}
        data={users}
        enableColumnFilterModes={false}
        enableRowNumbers
        enableFullScreenToggle={false}
        enableHiding={false}
        enableColumnActions={false}
        initialState={{
          showColumnFilters: false,
          showGlobalFilter: true,
          pagination: { pageSize: 5, pageIndex: 0 },
          density: "sm",
        }}
        positionToolbarAlertBanner="bottom"
        positionGlobalFilter="left"
        mantineSearchTextInputProps={{
          placeholder: `Search all users`,
          sx: { minWidth: "300px" },
          variant: "filled",
        }}
      />
    );
};
