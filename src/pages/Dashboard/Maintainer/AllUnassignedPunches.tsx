import { FC, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { GetAllForMaintainer } from "../../../services/punch";
import { ItemList } from "../../../components/FacilitySummarySection/SimpleTable";
import { IPoint } from "../../../components/Drawing/IPoint";

export const AllUnsassignedPunches: FC = () => {
  const [punches, setPunches] = useState<IPoint[]>([]);
  const { facilityId, spaceId } = useParams();

  useEffect(() => {
    GetAllForMaintainer(null, Number(facilityId), Number(spaceId), true).then(
      (f) => {
        setPunches(f);
      }
    );
  }, []);

  return (
    <>
      <ItemList
        items={punches}
        setItems={setPunches}
        title={"Unassigned punches"}
        name="punches"
        limit={5}
      />
    </>
  );
};
