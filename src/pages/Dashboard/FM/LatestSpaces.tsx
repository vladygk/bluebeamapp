import { FC, useContext } from "react";
import { Link } from "react-router-dom";
import { IFetchedSpace } from "../../../services/SpaceServices";
import { ThemeContext } from "../../../contexts";

export const LatestSpaces: FC<{ spaces: IFetchedSpace[] }> = (props: {
  spaces: IFetchedSpace[];
}) => {
  const { theme } = useContext(ThemeContext);

  return (
    <div className="flex justify-between my-10 py-5 rounded-lg md:flex-col md:justify-center md:items-center w-[60%] md:w-[80%] md:my-0 pt-0">
      {props.spaces.length > 0 ? (
        props.spaces.map((space) => (
          <Link
            key={space.id}
            to={`spaces/${space.id}`}
            className="cursor-default"
          >
            <img
              draggable={false}
              className={`object-contain mr-10 border-2 border-warning rounded-md w-[260px] h-[200px] cursor-pointer md:w-[360px] md:h-[300px]  md:mr-0 md:mt-5 sm:w-[260px] sm:h-[200px] ${
                theme == "darkTheme" && "dark-img"
              }`}
              src={space.imageUrl}
              alt={space.name}
            />
          </Link>
        ))
      ) : (
        <div className="flex-grow flex justify-center items-center">
          <p className="text-lg ">
            Currently there are no spaces in this facility.
          </p>
        </div>
      )}

      <div className="buttons pl-5 flex flex-col justify-center gap-5 md:mt-5 md:pl-0">
        <Link to="./space/add" className="btn btn-secondary">
          Add space
        </Link>
        <Link to="./spaces" className="btn btn-outline btn-secondary">
          View all
        </Link>
      </div>
    </div>
  );
};
