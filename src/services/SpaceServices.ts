import { IValueSpaceBody } from "../components/SpaceForm/SpaceForm";
import { getUserFromStorage } from "../utils/localStorage";

export interface IFetchedSpace {
  id: number;
  name: string;
  location: string;
  imageUrl: string;
  facilityId: number;
}

const baseUrl = "https://bluebean.azurewebsites.net/spaces";

export const createSpace = async (spaceData: IValueSpaceBody) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const body = JSON.stringify(spaceData);
  const response = await fetch(`${baseUrl}`, {
    method: "POST",
    body: body,
    headers: {
      "Content-Type": "application/json",
      Authorization: jwt,
    },
  });

  if (!response.ok) throw new Error("Failed to add space");

  //if()
  //const data = await response.json();
  return "Space created";
};

export const getAllSpaces = async () => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });

  const data: IFetchedSpace[] = await response.json();
  return data;
};

export const getAllSpacesForFacility = async (facilityId: number) => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?facilityId=${facilityId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });

  const data: IFetchedSpace[] = await response.json();
  return data;
};

export const getOneSpace = async (
  spaceId: number,
  facilityId: number
): Promise<any> => {
  const jwt = getUserFromStorage()?.token;

  if (!jwt) {
    return;
  }

  const response = await fetch(`${baseUrl}?spaceId=${spaceId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
      Authorization: jwt,
    },
  });
  if (!response.ok) throw new Error("Failed to fetch space");

  const data: IFetchedSpace = await response.json();

  if (data.facilityId != facilityId) {
    throw new Error("Incorrect facility");
  }
  return data;
};
