import { FC, useContext, useState } from "react";
import { Button } from "semantic-ui-react";
import { FaWindowClose } from "react-icons/fa";
import { RxDividerVertical } from "react-icons/rx";
import { Comments } from "../Comments";
import { PunchForm } from "../PunchForm/PunchForm";
import { SpaceContext } from "../../contexts/ISpaceContext";

export const PunchContainer: FC = () => {
  const [activeButton, setActiveButton] = useState<string | null>("details");
  const {
    isActiveForm,
    setIsActiveForm,
    buttonType,
    setButtonType,
    clickedPointData,
  } = useContext(SpaceContext)!;
  const [hasError, setHasError] = useState<boolean>(false);

  const renderDetails = (): void => {
    setButtonType("details");
    setActiveButton("details");
  };

  const renderComments = (): void => {
    setButtonType("comments");
    setActiveButton("comments");
  };

  const closeContainer = (): void => {
    setIsActiveForm(false);
    setActiveButton("details");
    setHasError(false);
  };

  return (
    <div
      className={`border-2 min-w-[30rem] md:min-w-0 sm:max-w-[calc(100vw-6.5rem)] sm:m-0 border-gray-400 px-4 sm:px-3 py-4 mx-auto rounded-md 2xl:w-[50%] lg:w-[60%] md:w-[90%] sm:w-full ${
        isActiveForm ? "" : "hidden"
      }`}
    >
      {buttonType != "add" && (
        <div className="flex justify-between items-center pb-4 h-[10%] sm:items-start">
          <div className="flex justify-start md:flex-wrap pl-5">
            <Button
              className={` btn text-accent btn-ghost text-lg ${
                activeButton === "details" ? "bg-info" : ""
              } `}
              onClick={renderDetails}
            >
              Details
            </Button>
            <div className="text-[25px] self-center">
              <RxDividerVertical />
            </div>
            <Button
              className={`text-accent btn btn-ghost text-lg ${
                activeButton === "comments" ? "bg-info" : ""
              } `}
              onClick={renderComments}
            >
              Comments
            </Button>
          </div>
          <Button
            className="btn btn-ghost text-lg text-accent"
            onClick={closeContainer}
          >
            <FaWindowClose />
          </Button>
        </div>
      )}
      {buttonType == "details" && (
        <PunchForm hasError={hasError} setHasError={setHasError} />
      )}
      {clickedPointData && buttonType === "comments" && <Comments />}
      {buttonType == "add" && (
        <PunchForm hasError={hasError} setHasError={setHasError} />
      )}
    </div>
  );
};
