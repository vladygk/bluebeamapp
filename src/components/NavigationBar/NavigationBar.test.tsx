// import { render, screen } from "@testing-library/react";
// import { NavigationBar } from "./NavigationBar";
// import { BrowserRouter} from "react-router-dom";
// import "@testing-library/jest-dom";
// import { AuthContext, IAuthContext } from "../../contexts";

// const loggedUser: IAuthContext = {
//     userData: { email: "", accessToken: "" },
//     onRegister: (e, value, repeatPassword) => {
//         return new Promise(() => true);
//     },
//     onLogin: (e, value) => {
//         return new Promise(() => true);
//     },
//     onLogout: () => {
//         return new Promise(() => true);
//     },
// };

// const notLoggedUser: IAuthContext = {
//     userData: null,
//     onRegister: (e, value, repeatPassword) => {
//         return new Promise(() => true);
//     },
//     onLogin: (e, value) => {
//         return new Promise(() => true);
//     },
//     onLogout: () => {
//         return new Promise(() => true);
//     },
// };

// describe("Navigation bar functionallity", () => {
//     test("render logo", () => {
//         render(
//             <BrowserRouter>
//                 <NavigationBar />
//             </BrowserRouter>
//         );
//         const logoElement = screen.getByAltText("logo");
//         expect(logoElement).toBeInTheDocument();
//     });

//     test("display logout btn when user is logged in", () => {
//         const { queryByTestId } = render(
//             <BrowserRouter>
//                 <AuthContext.Provider value={loggedUser}>
//                     <NavigationBar />
//                 </AuthContext.Provider>
//             </BrowserRouter>
//         );

//         const logoutBtn = queryByTestId("logout");
//         expect(logoutBtn).toBeInTheDocument();
//         expect(logoutBtn).not.toHaveStyle({ display: "hidden" });
//     });

//     test("don't display logout btn when user is not logged in", () => {
//         const { queryByTestId } = render(
//             <BrowserRouter>
//                 <AuthContext.Provider value={notLoggedUser}>
//                     <NavigationBar />
//                 </AuthContext.Provider>
//             </BrowserRouter>
//         );

//         const logoutBtn = queryByTestId("logout");
//         expect(logoutBtn).not.toBeInTheDocument();
//     });
// });
