import { useEffect, useState } from "react";
import { Input } from "../Input";
import { useNavigate } from "react-router-dom";
import { RiLinksLine } from "react-icons/ri";
import convertFileToString from "../../utils/convertFileToString";
import { createFacility } from "../../services/FacilityServices";
import { getUserIdFromStorage } from "../../utils/localStorage";

export interface IValueFacilityForm {
  name: string;
  city: string;
  address: string;
  photo: File | null;
}

export interface IValueFacilityBody {
  Name: string;
  City: string;
  Address: string;

  ImageUrl: string;
  creatorId: string;
}

export const FacilityForm = () => {
  const [formState, setFormState] = useState<IValueFacilityForm>({
    name: "",
    city: "",
    address: "",
    photo: null,
  });

  const [facilityBody, setFacilitybody] = useState<IValueFacilityBody>({
    Name: "",
    City: "",
    Address: "",

    ImageUrl: "",
    creatorId: "",
  });

  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setHasError(false);
    };
  }, []);

  const navigate = useNavigate();

  const createFacilityBody = async (
    values: IValueFacilityForm,
    base64Photo: string
  ) => {
    setFacilitybody((prevState) => ({
      ...prevState,
      Name: values.name,
      City: values.city,
      Address: values.address,

      ImageUrl: base64Photo,
    }));

    const updatedFacilityBody = {
      ...facilityBody,
      Name: values.name,
      City: values.city,
      Address: values.address,

      ImageUrl: base64Photo,
      creatorId: getUserIdFromStorage(),
    };

    await setFacilitybody(updatedFacilityBody);

    return updatedFacilityBody;
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    const base64code: string | undefined = await convertFileToString(event);
    if (base64code === undefined) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    const test = await createFacilityBody(formState, base64code);

    // Form validation
    if (Object.values(test).some((x) => x === "")) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await createFacility(test);

    // redirect
    navigate(`/`);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0] || null;
    setFormState((prevState) => ({
      ...prevState,
      photo: file,
    }));
  };

  return (
    <>
      <div className="w-2/5 border-2 border-info px-7 py-5 my-20 mx-auto md:my-20 rounded-md md:w-[80%] sm:w-[90%]">
        <form data-testid="form" onSubmit={handleSubmit}>
          <h2 className="m-6 text-center text-2xl text-accent font-bold">
            Add Facility
          </h2>
          <Input
            labelText="Name:"
            type="text"
            placeholder="Facility name"
            name="name"
            onChange={handleInputChange}
            value={formState["name"]}
          ></Input>
          <Input
            labelText="City:"
            type="text"
            placeholder="City"
            name="city"
            onChange={handleInputChange}
            value={formState["city"]}
          ></Input>
          <Input
            labelText="Address:"
            type="text"
            placeholder="Address"
            name="address"
            onChange={handleInputChange}
            value={formState["address"]}
          ></Input>

          <label className="font-bold cursor-pointer" htmlFor="photo">
            <div className="btn bg-secondary hover:text-primary my-5 ">
              <RiLinksLine />
              Upload Photo
            </div>
          </label>
          <input
            className="hidden"
            type="file"
            placeholder="Upload photo"
            name="photo"
            id="photo"
            onChange={handleFileChange}
            accept="image/*"
          ></input>
          <p>{formState.photo ? formState.photo.name : ""}</p>
          {hasError && (
            <span className="text-error">All fields must be filled</span>
          )}
          <button className="btn btn-secondary w-full my-5 " type="submit">
            Submit
          </button>
        </form>
      </div>
    </>
  );
};
