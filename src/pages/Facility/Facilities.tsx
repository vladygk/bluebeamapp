import { FC, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { FacilityCard } from "./FacilityCard";

import {
  IFetchedFacility,
  getAllFacilitiesForUser,
} from "../../services/FacilityServices";
import { AuthContext } from "../../contexts";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";

export const FacilityPage: FC = () => {
  const [facilities, setFacilities] = useState<IFetchedFacility[] | null>([]);
  const { userId, userRole } = useContext(AuthContext);

  useEffect(() => {
    getAllFacilitiesForUser(Number(userId))
      .then((f) => {
        if (f !== undefined) {
          setFacilities([...f]);
          return f;
        }
      })
      .then((f) => {
        if (f!.length == 0) setFacilities(null);
      })
      .catch((e) => console.log(e.message));
  }, [userId]);

  return (
    <div className="px-24 py-10 md:px-14 sm:px-10 min-h-screen sm:w-full">
      <div className="flex justify-between items-center pb-10 rounded-md sm:flex-col">
        <h1 className="text-left text-accent my-[10px] text-[3rem] font-bold">
          Facilities
        </h1>
        {userRole == "FM" && (
          <Link to={"/facilities/add"} className="btn btn-secondary text-lg">
            Add facility
          </Link>
        )}
      </div>
      {facilities?.length == null ? (
        <p className="text-2xl text-accent">
          There are currently no facilities that you're assigned to.
          <br /> Contact your FM for more information.
        </p>
      ) : facilities.length === 0 ? (
        <LoadingScreen />
      ) : (
        <div className="grid grid-cols-3 gap-[3rem] md:grid-cols-2 md:gap-[2rem] sm:grid-cols-1 sm:gap-[1rem]">
          {facilities.map((element: IFetchedFacility) => (
            <FacilityCard
              id={element.id}
              key={element.id}
              name={element.name}
              city={element.city}
              address={element.address}
              imageUrl={element.imageUrl}
            />
          ))}
        </div>
      )}
    </div>
  );
};
