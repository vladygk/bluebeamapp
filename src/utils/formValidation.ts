import { Dispatch } from "react";

export interface IHasError {
  email: boolean;
  password: boolean;
  name?: boolean;
  repeatPassword?: boolean;
}

export interface IErrorMessage {
  name: string;
  email: string;
  password: string;
  repeatPassword: string;
}

export interface IErrorCriteria {
  name: RegExp;
  email: RegExp;
  password: RegExp;
}

export const validationCriteria: IErrorCriteria = {
  email: /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/,
  name: /\w{2,} \w{2,}/,
  password: /.{6,}/,
};

export const errorMessages: IErrorMessage = {
  email: "Invalid email",
  name: "Please enter at least two names",
  password: "Password must be at least 6 symbols",
  repeatPassword: "Passwords don't match",
};

export const validate = (
  value: string,
  key: string,
  setError: Dispatch<React.SetStateAction<IHasError>>
) => {
  if (!value.match(validationCriteria[key as keyof IErrorCriteria])) {
    setError((state) => ({ ...state, [key]: true }));
  } else {
    setError((state) => ({ ...state, [key]: false }));
  }
};
