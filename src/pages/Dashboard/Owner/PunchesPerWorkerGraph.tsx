import { FC } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ReferenceLine,
  ResponsiveContainer,
  Label,
} from "recharts";
import { IFetchedUser } from "../../../services/auth";
import { IPoint } from "../../../components/Drawing/IPoint";

interface IPunchesPerWorkerGraph {
  workers: IFetchedUser[];
  punches: IPoint[];
}

interface IPunchesPerWorkerGraphData {
  name: string;
  PunchesInProgressCount: number;
  CompletedPunchesCount: number;
}

interface IPunchesPerWorkerGraph {
  workers: IFetchedUser[];
  punches: IPoint[];
}

const getPunchesPerWorkerData = (
  workers: IFetchedUser[],
  punches: IPoint[]
): IPunchesPerWorkerGraphData[] => {
  const workerPunches: IPunchesPerWorkerGraphData[] = [];

  workers.forEach((worker) => {
    const punchesInProgressCount = punches.filter(
      (punch) =>
        punch.assignee === worker.name && punch.status === "In progress"
    ).length;

    const completedPunchesCount = punches.filter(
      (punch) => punch.assignee === worker.name && punch.status === "Completed"
    ).length;

    workerPunches.push({
      name: worker.name,
      PunchesInProgressCount: punchesInProgressCount,
      CompletedPunchesCount: completedPunchesCount,
    });
  });

  return workerPunches;
};

export const PunchesPerWorkerGraph: FC<IPunchesPerWorkerGraph> = ({
  workers,
  punches,
}) => {
  const data: IPunchesPerWorkerGraphData[] = getPunchesPerWorkerData(
    workers,
    punches
  );

  const legendFormatter = (value: string) => {
    if (value === "PunchesInProgressCount") {
      return "Punches In Progress";
    } else if (value === "CompletedPunchesCount") {
      return "Completed Punches";
    }
    return value;
  };

  return (
    <div className="w-[80%] sm:w-full">
      <h3 className="text-xl my-10 font-bold">Punches per Worker</h3>
      <div className="w-full sm:w-full">
        <ResponsiveContainer width="100%" height={300}>
          <BarChart
            width={500}
            height={300}
            data={data}
            stackOffset="sign"
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name">
              <Label value="Worker" position="insideBottomRight" offset={-10} />
            </XAxis>
            <YAxis allowDecimals={false}>
              <Label
                value="Punches"
                position="insideLeft"
                angle={-90}
                offset={10}
              />
            </YAxis>
            <Tooltip />
            <Legend formatter={legendFormatter} />
            <ReferenceLine y={0} stroke="#000" />
            <Bar
              dataKey="PunchesInProgressCount"
              fill="#8884d8"
              stackId="stack"
              barSize={70}
            />
            <Bar
              dataKey="CompletedPunchesCount"
              fill="#82ca9d"
              stackId="stack"
              barSize={70}
            />
          </BarChart>
        </ResponsiveContainer>
      </div>
    </div>
  );
};
