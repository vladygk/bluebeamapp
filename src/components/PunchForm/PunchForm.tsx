import React, { useCallback } from "react";
import { FC, useContext, useState, MouseEvent, useEffect } from "react";
import { Input } from "../../components/Input/Input";
import { FaEdit } from "react-icons/fa";

import { create, edit, remove } from "../../services/punch";
import { useParams } from "react-router-dom";
import { IPoint } from "../Drawing/IPoint";
import { getAllAssetsForFacility } from "../../services/asset";

import { getAllStatuses } from "../../services/status";
import { SpaceContext } from "../../contexts/ISpaceContext";
import {
  getRoleFromStorage,
  getUserIdFromStorage,
} from "../../utils/localStorage";
import { getAllUsersWithRoleFromFacility } from "../../services/auth";

interface IPunchContainerData {
  hasError: boolean;
  setHasError: React.Dispatch<React.SetStateAction<boolean>>;
}

export const PunchForm: FC<IPunchContainerData> = ({
  hasError,
  setHasError,
}) => {
  interface IUserName {
    id: number;
    name: string;
  }

  interface IStatus {
    id: number;
    name: string;
  }
  const { facilityId, spaceId } = useParams();
  const [assetsForFacility, setAssetsForFacility] = useState<any>([]);
  const [users, setUsers] = useState<IUserName[]>([]);
  const [statuses, setStatuses] = useState<IStatus[]>([]);
  const {
    setIsActiveForm,
    isActiveForm,
    buttonType,
    points,
    setPoints,
    clickedPointData,
  } = useContext(SpaceContext)!;

  const [formValues, setFormValues] = useState<IPoint>({
    id: 77,
    title: "",
    description: "",
    assigneeId: null,
    assignee: null, // Name
    assetId: null,
    asset: "-1", // important for validation
    expirationDate: "",
    startDate: "-1", // important for validation
    spaceId: "",
    creatorId: "1",
    status: "Unassinged",
    coordX: -1,
    coordY: -1,
    statusId: -1,
    isClicked: false,
  });

  const [editMode, setToEditMode] = useState<boolean>(false);
  const [canEdit, setCanEdit] = useState<boolean>(false);
  const [userRole, setUserRole] = useState<string>("");
  const [userId, setUserId] = useState<string>("");

  useEffect(() => {
    setUserRole(getRoleFromStorage());
    setUserId(getUserIdFromStorage());
  }, []);

  useEffect(() => {
    return () => {
      setHasError(false);
    };
  }, []);

  useEffect(() => {
    setFormValues(clickedPointData!);
  }, [clickedPointData, editMode]);

  const resetForm = useCallback(() => {
    setFormValues((state) => ({
      ...state,
      spaceId: spaceId!,
      id: 77,
      title: "",
      description: "",
      asset: "-1", // important for validation
      // Name
      expirationDate: "",
      startDate: "-1", // important for validation
      status: "Unassinged",
      coordX: -1,
      coordY: -1,
    }));
  }, [spaceId]);

  useEffect(() => {
    if (!isActiveForm) setToEditMode(false);
    if (isActiveForm && buttonType === "add") resetForm();
  }, [isActiveForm, buttonType, resetForm]);

  useEffect(() => {
    setFormValues((state) => ({ ...state, spaceId: spaceId! }));
  }, [editMode, spaceId]);

  useEffect(() => {
    getAllAssetsForFacility(Number(facilityId)).then((x) =>
      setAssetsForFacility(x)
    );

    getAllUsersWithRoleFromFacility(Number(facilityId), 3).then((u) =>
      setUsers(u)
    );

    getAllStatuses().then((s) => setStatuses(s));
  }, [facilityId]);

  useEffect(() => {
    setCanEdit(
      userRole.toLowerCase() === "fm" ||
        clickedPointData.assigneeId == Number(userId) ||
        clickedPointData.creatorId == userId
    );
  }, [clickedPointData, userId, userRole]);

  const handleSetToEditMode = (e: MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    setToEditMode(!editMode);
  };

  const formatDate = (date: string): string => {
    if (!date) return "";

    console.log("Raw date", date);

    if (date.includes("/")) {
      // Format: 6/7/2023 11:10:59 AM
      let [month, day, year] = date.split("/");
      year = year.split(" ")[0].split("-").toString();
      const formattedMonth = month.length === 1 ? `0${month}` : month;
      const formattedDay = day.length === 1 ? `0${day}` : day;
      console.log(
        "Formatted date:",
        `${year}-${formattedMonth}-${formattedDay}`
      );
      return `${year}-${formattedMonth}-${formattedDay}`;
    } else if (date.includes("-")) {
      // Format: 2023-06-07 11:08:35.9000000
      const dateParts = date.split(" ")[0];
      const [year, month, day] = dateParts.split("-");
      console.log("Formatted date:", `${year}-${month}-${day}`);
      return `${year}-${month}-${day}`;
    } else {
      return ""; // Invalid date format
    }
  };

  const handleChange = (
    e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>
  ) => {
    const { name, value } = e.target;

    setFormValues((state) => ({ ...state, [name]: value }));
  };

  useEffect(() => {
    console.log(clickedPointData);
  }, [clickedPointData]);

  const handleCancel = (e: any) => {
    e.preventDefault();

    setIsActiveForm(false);
    setPoints((points) => [...points.slice(0, -1)]);
    setHasError(false);
    resetForm();
    // Decrease POINT COUNT -1
  };

  const handleAddPunch = async (e: any): Promise<void> => {
    e.preventDefault();

    const point = points[points.length - 1];

    // Form validation
    if (Object.values(formValues).some((x) => x === "")) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await create({
      ...formValues,

      coordX: point.coordX,
      coordY: point.coordY,
      statusId: 2,
      creatorId: userId,
    });
    setIsActiveForm(false);
    resetForm();
  };

  const onInputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) =>
    setFormValues((state) => ({
      ...state,
      [e.target.name]: e.target.value,
    }));

  const handleSelectChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectOption =
      Number(e.target.value) == -1 ? null : Number(e.target.value);

    setFormValues((prevState) => ({
      ...prevState,
      [e.target.name]: selectOption,
    }));
  };

  const handleComplete = async (e: any) => {
    e.preventDefault();
    const pointToEditId = clickedPointData!.id;

    await edit({ ...formValues, statusId: 3 }, pointToEditId);
    setIsActiveForm(false);
  };

  const handleDelete = async (e: any) => {
    e.preventDefault();
    const pointToDeleteId = clickedPointData!.id;
    await remove(pointToDeleteId);
    setIsActiveForm(false);
  };

  const handleSave = async (e: any) => {
    e.preventDefault();
    const pointToEditId = clickedPointData!.id;

    // Form validation
    if (Object.values(formValues).some((x) => x === "")) {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await edit(formValues, pointToEditId);
    setIsActiveForm(false);
  };

  const handleBack = (e: any) => {
    e.preventDefault();
    setToEditMode(false);
    setHasError(false);
  };

  const handleAssign = async (e: any) => {
    e.preventDefault();

    const pointToEditId = clickedPointData!.id;

    await edit({ ...formValues, assigneeId: Number(userId) }, pointToEditId);

    resetForm();
    setIsActiveForm(false);
  };

  return (
    <div
      className={`border-2 px-7 py-7 mx-auto w-[100%] rounded-md sm:w-full ${
        editMode ? "border-secondary" : "border-info"
      }`}
    >
      <form data-testid="form" method="POST">
        <div className="flex justify-between">
          <h2 className="text-center text-accent text-2xl font-bold">
            {(() => {
              if (buttonType === "details" && editMode) {
                return "Edit";
              } else if (buttonType === "details") {
                return "Details";
              } else if (buttonType === "add") {
                return "Punching";
              }
            })()}
          </h2>
          {hasError && (
            <span className="text-error">All fields must be filled</span>
          )}
          {canEdit && (
            <button
              className={`${buttonType === "add" ? "hidden" : null}`}
              onClick={handleSetToEditMode}
            >
              <FaEdit className="text-[27px] text-accent"></FaEdit>
            </button>
          )}
        </div>
        <Input
          disabled={!editMode && buttonType === "details"}
          labelText="Title"
          name="title"
          type="text"
          placeholder="Punch title"
          onChange={handleChange}
          value={!editMode && formValues ? formValues.title : undefined}
        ></Input>
        <Input
          disabled={!editMode && buttonType === "details"}
          labelText="Description"
          name="description"
          type="text"
          placeholder="Punch description"
          onChange={onInputChangeHandler}
          value={!editMode && formValues ? formValues.description : undefined}
        ></Input>
        {editMode === true || buttonType === "add" ? (
          <>
            <label htmlFor="assignee" className="font-bold text-accent">
              Assignee
            </label>
            <select
              onChange={handleSelectChange}
              value={formValues.assigneeId ?? -1}
              className="border-2 border-info  w-full mb-5 rounded-md h-10 pl-2 bg-primary text-accent"
              id="assignee"
              name="assigneeId"
            >
              <option key={1000} value={-1}>
                Unassigned
              </option>
              {users.map((user: IUserName) => {
                return (
                  <option key={user.id} value={user.id}>
                    {user.name}
                  </option>
                );
              })}
            </select>
          </>
        ) : (
          <Input
            labelText="Assignee"
            disabled
            name="assignee"
            type="text"
            placeholder="Assignee"
            onChange={onInputChangeHandler}
            value={
              !editMode &&
              buttonType === "details" &&
              formValues &&
              formValues.assignee
                ? formValues.assignee
                : "Unassigned"
            }
          ></Input>
        )}
        {editMode === true || buttonType === "add" ? (
          <>
            <label htmlFor="asset-edit " className="font-bold text-accent">
              Asset
            </label>
            <select
              onChange={handleSelectChange}
              value={formValues.assetId ?? -1}
              className="border-2 border-info  w-full rounded-md h-10 pl-2 bg-primary text-accent"
              id="asset-edit"
              name="assetId"
            >
              <option key={1000} value={-1}>
                None
              </option>
              {assetsForFacility.map((asset: any /* asset interface*/) => {
                return (
                  <option key={asset.id} value={asset.id}>
                    {asset.name}
                  </option>
                );
              })}
            </select>
          </>
        ) : (
          <Input
            labelText="Asset"
            disabled
            name="asset-details"
            type="text"
            placeholder="Asset"
            onChange={onInputChangeHandler}
            value={
              !editMode && formValues && formValues.asset
                ? formValues.asset
                : "No asset"
            }
          ></Input>
        )}
        <Input
          hidden={buttonType === "add"}
          disabled={!editMode && buttonType === "details"}
          labelText="Created on"
          name="startDate"
          type="date"
          placeholder="Created on"
          onChange={handleChange}
          value={formatDate(formValues.startDate)}
        ></Input>
        <Input
          disabled={!editMode && buttonType === "details"}
          labelText="End Date"
          name="expirationDate"
          type="date"
          placeholder="End Date"
          onChange={handleChange}
          value={
            buttonType === "add"
              ? formValues.expirationDate
              : formatDate(formValues.expirationDate)
          }
        ></Input>
        {editMode === true ? (
          <>
            <label htmlFor="status" className="font-bold text-accent">
              Status{" "}
            </label>
            <select
              onChange={handleSelectChange}
              value={formValues.statusId}
              className="border-2 w-full border-info  mb-5 rounded-md h-10 pl-2 bg-primary text-accent"
              id="status"
              name="statusId"
            >
              {statuses.map((status: IStatus) => {
                return (
                  <option key={status.id} value={status.id}>
                    {status.name}
                  </option>
                );
              })}
            </select>
          </>
        ) : (
          <Input
            hidden={buttonType === "add"}
            disabled={!editMode && buttonType === "details"}
            labelText="Status"
            name="statusId"
            type="text"
            placeholder="Status"
            value={!editMode && formValues ? formValues.status : undefined}
          ></Input>
        )}
        <div
          className={`add-btns flex justify-between lg:flex-wrap sm:justify-center sm:items-center sm:flex-col-reverse ${
            buttonType !== "add" ? "hidden" : null
          } `}
        >
          <button
            onClick={handleCancel}
            className="btn w-[7rem] my-2 mx-2 btn-secondary"
          >
            Cancel
          </button>{" "}
          <button
            onClick={handleAddPunch}
            className="btn w-[7rem] my-2 mx-2 btn-secondary"
          >
            Add punch
          </button>
          {/* Add punch -to make a new punch and to lead to Space page - no active schematic*/}
        </div>
        <div
          className={`details-btns flex justify-between sm:justify-center md:flex-wrap md:gap-5 sm:items-center sm:flex-col-reverse ${
            buttonType !== "details" || (buttonType === "details" && editMode)
              ? "hidden"
              : ""
          } `}
        >
          {userRole.toLowerCase() === "fm" && (
            <button
              onClick={handleDelete}
              className="btn  w-[7rem] sm:mt-5 sm:w-[6rem] btn-secondary"
            >
              Delete
            </button>
          )}
          {userRole.toLowerCase() === "worker" &&
            clickedPointData.statusId === 2 && (
              <button
                onClick={handleAssign}
                className="btn w-[7rem] sm:w-[6rem] btn-secondary"
              >
                Assign to me
              </button>
            )}
          {(Number(userId) == clickedPointData.assigneeId ||
            userRole.toLowerCase() === "fm") && (
            <button
              onClick={handleComplete}
              className="btn w-[7rem] sm:w-[6rem] btn-secondary"
              disabled={clickedPointData.statusId == 3 ? true : false}
            >
              Complete
            </button>
          )}
        </div>
        <div
          className={`edit-btns  flex justify-between lg:flex-wrap sm:justify-center  sm:items-center sm:flex-col-reverse ${
            buttonType !== "details" || !editMode ? "hidden" : null
          } `}
        >
          <button
            onClick={handleBack}
            className="btn w-[7rem] sm:mt-5 sm:w-[6rem] btn-secondary"
          >
            Back
          </button>
          <button
            onClick={handleSave}
            className="btn w-[7rem] sm:w-[6rem] btn-secondary"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );
};
