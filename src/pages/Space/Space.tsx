import { useContext, useEffect } from "react";
import { Drawing } from "../../components/Drawing";
import { Button } from "../../components/Button";
import { PunchContainer } from "../../components/PunchContainer";

import { useLocation, useParams } from "react-router-dom";

import { GetAllForMaintainer, getAllForSpace } from "../../services/punch";
import { SpaceContext } from "../../contexts/ISpaceContext";
import {
  getRoleFromStorage,
  getUserIdFromStorage,
} from "../../utils/localStorage";

export const Space = () => {
  const {
    setIsActiveForm,
    isActiveForm,

    setButtonType,

    isDrawable,
    setIsDrawable,
    setPoints,

    setClickedPointData,
  } = useContext(SpaceContext)!;
  const { facilityId, spaceId } = useParams();
  const { state } = useLocation();

  useEffect(() => {
    if (!isActiveForm) {
      const userRole = getRoleFromStorage();
      const userId = getUserIdFromStorage();
      if (userRole.toLowerCase() === "worker") {
        GetAllForMaintainer(userId, Number(facilityId), Number(spaceId)).then(
          (x) => setPoints(x)
        );
      } else {
        getAllForSpace(Number(spaceId)).then((x) => setPoints(x));
      }
    } // Get points
  }, [isActiveForm]);

  useEffect(() => {
    if (state) {
      setClickedPointData(state);
      setButtonType("details");
      setIsActiveForm(true);
      // points.find((point)=> point.id === state.id)
    }

    return () => {
      setPoints([]);
    };
  }, []);

  useEffect(() => {
    return () => {
      setPoints([]);
    };
  }, []);

  const handleClick = (): void => {
    setButtonType("add");
    setIsDrawable(true);
  };

  const handleCancelClick = (): void => {
    setIsDrawable(false);
  };

  return (
    <div className="mb-10">
      <div
        className={`flex justify-end sm:justify-end align-center mb-2 text-4xl font-bold ${
          isActiveForm ? "hidden" : ""
        }`}
      >
        <Button
          customClass={`w-[110px] md:w-[80px] sm:mb-5 sm:w-[120px] ${
            isDrawable ? "" : "bg-secondary"
          }`}
          onClick={isDrawable ? handleCancelClick : handleClick}
          text={`${isDrawable ? "Cancel" : "Add punch"}`}
        ></Button>
      </div>
      <div
        className={`flex sm:justify-center ${
          isActiveForm ? "justify-between 2xl:flex-col" : "justify-start"
        }`}
      >
        <div className="mb-10">
          <Drawing />
        </div>
        <div className={!isActiveForm ? "hidden" : ""}>
          <PunchContainer />
        </div>
      </div>
    </div>
  );
};
