import { FC } from "react";

interface IRadialProgress {
  title: string;
  punchByStatusInPercent: number | null;
  color: string;
}

interface IRadialProgressStyle extends React.CSSProperties {
  "--value"?: number;
}

export const RadialProgress: FC<IRadialProgress> = ({
  title,
  punchByStatusInPercent,
  color,
}) => {
  return (
    <div className="flex flex-col items-center">
      <p className="badge badge-secondary p-3 mb-10">{title}</p>
      <div
        className={`radial-progress ${color}`}
        style={
          {
            "--size": "7rem",
            "--value": punchByStatusInPercent,
          } as IRadialProgressStyle
        }
      >
        {punchByStatusInPercent || punchByStatusInPercent == 0
          ? `${punchByStatusInPercent}%`
          : "Loading..."}
      </div>
    </div>
  );
};
