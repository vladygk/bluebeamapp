import { FC, useContext } from "react";
import { useNavigate } from "react-router-dom";

import { AuthContext, ThemeContext } from "../../contexts/";

import photo from "../../assets/photos/home.jpg";
import { FacilityPage } from "../Facility";

export const Home: FC = () => {
  const { userData } = useContext(AuthContext);

  const navigate = useNavigate();
  const { theme } = useContext(ThemeContext);
  return (
    <>
      {userData && <FacilityPage />}
      {!userData && (
        <div
          data-testid="home-div"
          className={`w-full ${
            theme === "darkTheme" && "dark-img"
          } min-h-[100vh] hero sm:min-h-[100svh]`}
          style={{ backgroundImage: `url(${photo})` }}
        >
          <div
            className={`hero-overlay bg-opacity-${
              theme === "lightTheme" ? 30 : 10
            }`}
          ></div>
          <div className="hero-content text-center text-neutral-content">
            <div
              className={`max-w-lg  ${theme === "darkTheme" && "dark-img"} `}
            >
              <h1 className="mb-5  text-5xl leading-[3.5rem] drop-shadow-xl font-bold text-white">
                {" "}
                Managing facilities made easy.
              </h1>
              <p className="mb-5  text-lg font-semibold drop-shadow-xl text-white">
                Improving your success through technology.
              </p>
              <button
                className="btn  btn-secondary btn-lg mt-6 drop-shadow-xl shadow-xl"
                onClick={() => navigate("/register")}
              >
                Get Started
              </button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
