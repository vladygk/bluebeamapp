import { useContext } from "react";
import { UsersTable } from "../../components/UsersTable";
import { Link } from "react-router-dom";
import { AuthContext } from "../../contexts";

export const Users = () => {
  const { userRole } = useContext(AuthContext);

  return (
    <div className="w-[90%] mx-16 my-8 min-h-[calc(100svh-88px)] max-w-[calc(100svw-10.5rem)] md:w-[95%] md:mx-12 sm:mx-0 sm:px-5 sm:mb-5 sm:w-full sm:max-w-full ">
      <div className="flex justify-between items-center sm:flex-col sm:text-center sm:items-center">
        <h1 className="text-left my-[10px] mb-12 text-accent text-[3rem] font-bold sm:mb-10">
          Facility's users
        </h1>
        {userRole == "FM" && (
          <Link
            to="./add"
            className="btn btn-secondary text-lg sm:w-[10rem] sm:mb-10"
          >
            Add User
          </Link>
        )}
      </div>
      <UsersTable />
    </div>
  );
};
