import { useEffect, useState, FC, Suspense } from "react";
import { useParams } from "react-router-dom";
import { GetAllForFacility } from "../../../services/punch";
import { IPoint } from "../../../components/Drawing/IPoint";
import { PunchPerDateGraph } from "./PunchPerDateGraph";
import { PunchesPerWorkerGraph } from "./PunchesPerWorkerGraph";
import {
  IFetchedUser,
  getAllUsersWithRoleFromFacility,
} from "../../../services/auth";
import { QuickStats } from "./QuickStats";
import { LoadingScreen } from "../../LoadingScreen/LoadingScreen";

export const OwnerDashboard: FC = () => {
  const { facilityId } = useParams();
  const [punches, setPunches] = useState<IPoint[] | null>(null);
  const [workers, setWorkers] = useState<IFetchedUser[] | null>(null);

  if (!facilityId) {
    return <h1>error...</h1>;
  }

  useEffect(() => {
    const fetchPunches = async () => {
      try {
        const punchesData = await GetAllForFacility(+facilityId);
        if (!punchesData) {
          throw new Error("There is no data for the given Facility Id");
        }
        setPunches(punchesData);
      } catch (error) {
        console.error("Error while fetching punches:", error);
      }
    };

    const fetchWorkers = async () => {
      try {
        const workersData = await getAllUsersWithRoleFromFacility(
          +facilityId,
          3
        );
        if (!workersData) {
          throw new Error("There is no workers data for the given Facility Id");
        }
        setWorkers(workersData);
      } catch (error) {
        console.error("Error while fetching workers:", error);
      }
    };

    fetchPunches();
    fetchWorkers();
  }, [facilityId]);

  if (!punches || !workers) {
    return <LoadingScreen />;
  }

  return (
    <Suspense fallback={<LoadingScreen />}>
      <div className="px-10 mb-10 sm:px-5">
        <div className="flex flex-col items-center">
          {punches && <QuickStats punches={punches} />}
          {punches && <PunchPerDateGraph punches={punches} />}
          {punches && workers && (
            <PunchesPerWorkerGraph punches={punches} workers={workers} />
          )}
        </div>
      </div>
    </Suspense>
  );
};
