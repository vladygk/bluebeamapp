import { FC, useContext, useEffect, useRef, useState } from "react";
import { CommentCard, ICommentCard } from "./CommentCard";
import { Button } from "semantic-ui-react";
import { AuthContext } from "../../contexts";
import { createComment, getAllForPunch } from "../../services/comment";
import { SpaceContext } from "../../contexts/ISpaceContext";

export interface ICommentExport {
  punchId: number;
  userId: number;
  text: string;
  createdOn: string;
}

export const Comments: FC = () => {
  const [newCommentText, setNewCommentText] = useState<string>();
  const { userId } = useContext(AuthContext);
  const { clickedPointData } = useContext(SpaceContext)!;
  const containerRef = useRef<HTMLDivElement>(null);

  const [comments, setComments] = useState<ICommentCard[]>([]);

  useEffect(() => {
    // Scroll to the bottom of the container
    if (containerRef.current) {
      containerRef.current.scrollTop = containerRef.current.scrollHeight;
    }
  }, [comments]);

  useEffect(() => {
    getAllComments();
  }, []);

  const getAllComments = async () => {
    const fetchedComments = await getAllForPunch(clickedPointData.id);
    if (fetchedComments !== undefined) {
      setComments(fetchedComments);
    }
  };

  const handleAddComment = async () => {
    const newComment: ICommentExport = {
      userId: Number(userId),
      text: newCommentText!,
      createdOn:
        new Date().toLocaleDateString() +
        ", " +
        new Date().toLocaleTimeString([], {
          hour: "numeric",
          minute: "numeric",
        }),
      punchId: clickedPointData.id,
    };

    await createComment(newComment);
    setNewCommentText("");

    getAllComments();
  };

  return (
    <div className="flex flex-col border-info justify-between bg-primary w-full rounded-md border-2 p-3 h-[77vh]">
      <div className="flex flex-col overflow-y-auto" ref={containerRef}>
        {comments.length > 0 ? (
          comments.map((element) => (
            <CommentCard
              key={element.id}
              userName={element.userName}
              text={element.text}
              createdOn={element.createdOn}
            />
          ))
        ) : (
          <div className="flex items-center justify-center p-5">
            <p className="text-2xl">No comments.</p>
          </div>
        )}
      </div>
      <div className="flex items-end  justify-between rounded-md p-2">
        <div className="flex flex-col w-full ">
          <label className="font-bold text-accent" htmlFor="comment">
            Add Comment
          </label>
          <textarea
            className="w-full bg-primary border-info text-accent rounded-md h-20 p-3 break-words resize-none border-2 border-info"
            placeholder="Your comment here..."
            name="comment"
            id="comment"
            value={newCommentText}
            onChange={(e) => setNewCommentText(e.target.value)}
          ></textarea>
        </div>
        <Button className="btn btn-secondary ml-2" onClick={handleAddComment}>
          Add
        </Button>
      </div>
    </div>
  );
};
