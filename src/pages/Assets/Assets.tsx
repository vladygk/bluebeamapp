import { Link, useNavigate, useParams } from "react-router-dom";
import AssetsTable from "./AssetsTable";
import { useEffect } from "react";
import { getOneFacility } from "../../services/FacilityServices";

export const Assets = () => {
  const { facilityId } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch(() => navigate("/error"));
  }, [navigate, facilityId]);

  return (
    <div className="w-[90%] mx-16 my-8 min-h-[calc(100svh-88px)] max-w-[calc(100svw-10.5rem)] md:w-[95%] md:mx-12 sm:mx-0 sm:px-5 sm:mb-5 sm:w-full sm:max-w-full ">
      <div className="flex justify-between items-center sm:flex-col sm:text-center sm:items-center">
        <h1 className="text-left my-[10px] mb-12 text-accent text-[3rem] font-bold">
          Assets
        </h1>
        <Link to="./add" className="btn btn-secondary text-lg sm:mb-10">
          Add Asset
        </Link>
      </div>
      <AssetsTable />
    </div>
  );
};
