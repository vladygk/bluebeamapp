import { IPoint } from "../components/Drawing/IPoint";

const baseUrl = "https://bluebean.azurewebsites.net/punches";

export const create = async (punchData: IPoint) => {
  punchData.startDate = new Date().toISOString();

  punchData.status = "Unassigned";

  const body = JSON.stringify(punchData);

  await fetch(baseUrl, {
    method: "POST",
    headers: {
      "content-type": "application/json",
    },
    body: body,
  });

  return "Punch created";
};

export const edit = async (editpunchData: IPoint, id: number) => {
  const body = JSON.stringify(editpunchData);

  await fetch(`${baseUrl}/${id}`, {
    method: "PUT",
    headers: {
      "content-type": "application/json",
    },
    body: body,
  });

  return "Punch edited";
};

export const remove = async (id: number) => {
  await fetch(`${baseUrl}/${id}`, {
    method: "DELETE",
    headers: {
      "content-type": "application/json",
    },
  });

  //const data = await response.json();

  return "Punch deleted";
};

export const GetAll = async () => {
  const response = await fetch(baseUrl, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const getAllForSpace = async (spaceId: number) => {
  const response = await fetch(`${baseUrl}?spaceId=${spaceId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const GetAllForFacility = async (facilityId: number) => {
  const response = await fetch(`${baseUrl}?facilityId=${facilityId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const GetOne = async (punchId: number) => {
  const response = await fetch(`${baseUrl}?punchId=${punchId}`, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};

export const GetAllForMaintainer = async (
  userId: number | null,
  facilityId: number,
  spaceId?: number,
  isUnassigned?: boolean
) => {
  if (isUnassigned === undefined) isUnassigned = false;

  let url = `${baseUrl}?facilityId=${facilityId}`;
  if (spaceId) {
    url += `&spaceId=${spaceId}`;
  }
  if (userId) {
    url += `&userId=${userId}`;
  }
  if (isUnassigned !== null) {
    url += `&isUnassigned=${isUnassigned}`;
  }

  const response = await fetch(url, {
    method: "GET",
    headers: {
      "content-type": "application/json",
    },
  });

  const data = await response.json();

  return data;
};
