import { useState, useEffect } from "react";
import { Input } from "../Input";
import { useNavigate, useParams } from "react-router-dom";
import { createAsset } from "../../services/asset";
import { getOneFacility } from "../../services/FacilityServices";

interface IValueAssetForm {
  name: string;
}

export interface IValueAssetBody {
  Name: string;
  FacilityId: number;
  AddedOn: string;
}

export const AssetForm = () => {
  const [formState, setFormState] = useState<IValueAssetForm>({
    name: "",
  });
  const [hasError, setHasError] = useState<boolean>(false);

  useEffect(() => {
    return () => {
      setHasError(false);
    };
  }, []);

  const { facilityId } = useParams();
  const navigate = useNavigate();

  const formatDate = (date: string): string => {
    if (!date) return "";

    const dateParts = date.split(" ");

    const realDate = dateParts[0].split("-");
    const day = realDate[2];
    const month = realDate[1];
    const year = realDate[0];
    return `${year}-${month}-${day}`;
  };

  const handleSubmit = async (event: any) => {
    event.preventDefault();

    // Form validation
    if (formState.name === "") {
      return setHasError(true);
    } else {
      setHasError(false);
    }

    await createAsset({
      Name: formState.name,
      FacilityId: Number(facilityId),
      AddedOn: formatDate(new Date().toISOString().substring(0, 10)),
    });

    navigate(`/facility/${facilityId}/assets`);
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    setFormState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch(() => navigate("/error"));
  }, []);

  return (
    <>
      <div className="w-2/6 border-2 border-info px-7 mx-auto rounded-md m-[20px] md:w-[80%] sm:w-[90%]">
        <form onSubmit={handleSubmit}>
          <h2 className="m-6 text-center text-2xl font-bold text-accent">
            Add Asset
          </h2>
          <Input
            labelText="Name:"
            type="text"
            placeholder="Asset name"
            name="name"
            onChange={handleInputChange}
            value={formState["name"]}
          ></Input>
          {hasError && (
            <span className="text-error">All fields must be filled</span>
          )}
          <button className="btn btn-secondary w-full my-7 " type="submit">
            Submit
          </button>
        </form>
      </div>
    </>
  );
};
