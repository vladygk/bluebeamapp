import { FC, useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { SpaceCard } from "./SpaceCard";
import {
  getAllSpacesForFacility,
  IFetchedSpace,
} from "../../services/SpaceServices";
import { getOneFacility } from "../../services/FacilityServices";
import { getRoleFromStorage } from "../../utils/localStorage";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";
import noSpace from "../../assets/photos/noSpace.jpg";

export const Spaces: FC = () => {
  const [spaces, setSpaces] = useState<IFetchedSpace[] | null>([]);
  const { facilityId } = useParams();
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    getOneFacility(Number(facilityId)).catch(() => navigate("/error"));
    getAllSpacesForFacility(Number(facilityId))
      .then((f) => {
        if (f !== undefined) {
          setSpaces(f);
          return f;
        }
      })
      .then((s) => {
        if (s?.length == 0) {
          setSpaces(null);
        }
      });
    setIsLoading(false);
  }, [navigate, facilityId]);

  if (spaces === null) {
    return (
      <div className="px-24 py-10 md:px-14 sm:px-10  sm:w-full min-h-[calc(100vh-88px)]">
        <div className="flex flex-col items-center ">
          <h1 className="text-left my-[10px] text-accent text-[3rem] font-bold">
            No Spaces
          </h1>
          <img
            className=" w-[50%] rounded-md opacity-80 "
            src={noSpace}
            alt=""
          />
        </div>
      </div>
    );
  }

  if (isLoading) {
    return <LoadingScreen />;
  } else
    return (
      <div className="px-24 py-10 md:px-14 sm:px-10 min-h-screen sm:w-full">
        <div className="flex justify-between items-center pb-10 rounded-md sm:flex-col">
          <h1 className="text-left my-[10px] text-accent text-[3rem] font-bold">
            Spaces
          </h1>
          {getRoleFromStorage().toLowerCase() === "fm" && (
            <Link
              to={`/facility/${Number(facilityId)}/space/add`}
              className="btn btn-secondary text-lg"
            >
              Add space{" "}
            </Link>
          )}
        </div>

        <div className="grid grid-cols-3 gap-[3rem] md:grid-cols-2 md:gap-[2rem] sm:grid-cols-1 sm:gap-[1rem]">
          {spaces.map((element: IFetchedSpace) => (
            <SpaceCard
              id={element.id}
              key={element.id}
              name={element.name}
              location={element.location}
              imageUrl={element.imageUrl}
              facilityId={element.facilityId}
            />
          ))}
        </div>
      </div>
    );
};
