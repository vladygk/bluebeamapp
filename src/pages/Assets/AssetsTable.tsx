import { useEffect, useMemo, useState, useContext } from "react";

//MRT Imports
import { MantineReactTable, MRT_ColumnDef } from "mantine-react-table";

//Mantine Imports
import { Box } from "@mantine/core";

// Data
import { useParams } from "react-router-dom";
import {
  getAllAssetsForFacility,
  IFetchedAsset,
  removeAsset,
} from "../../services/asset";
import { DatePickerInput } from "@mantine/dates";

import { AuthContext, ThemeContext } from "../../contexts";
import { LoadingScreen } from "../LoadingScreen/LoadingScreen";

export const AssetsTable = () => {
  const [assets, setAssets] = useState<IFetchedAsset[]>([]);
  const [isSmallScreen, setIsSmallScreen] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const { facilityId } = useParams();
  const { userRole } = useContext(AuthContext);
  const { toggleMantineTable, theme } = useContext(ThemeContext);

  useEffect(() => {
    toggleMantineTable();
  }, [theme, toggleMantineTable, assets]);

  useEffect(() => {
    getAllAssetsForFacility(Number(facilityId)).then((f) => {
      if (f !== undefined) {
        setAssets(f.reverse());
      }
    });
    setIsLoading(false);
  }, []);

  useEffect(() => {
    const mediaQuery = window.matchMedia("(max-width: 1024px)");
    setIsSmallScreen(mediaQuery.matches);

    const handleMediaQueryChange = (e: MediaQueryListEvent) =>
      setIsSmallScreen(e.matches);
    mediaQuery.addEventListener("change", handleMediaQueryChange);

    // Remove event listener on unmount
    return () => {
      mediaQuery.removeEventListener("change", handleMediaQueryChange);
    };
  }, []);

  const handleAssetDelete = async (id: number) => {
    await removeAsset(id, Number(facilityId));
    setAssets((prevAssets) => prevAssets!.filter((asset) => asset.id !== id));
  };

  const columns = useMemo<MRT_ColumnDef<IFetchedAsset>[]>(
    () => [
      {
        id: "employee",
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.name}`,
            id: "title",
            header: "Asset Type",

            size: isSmallScreen ? 1 : 60, // Updated size based on isSmallScreen
            Cell: ({ renderedCellValue }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <span>{renderedCellValue}</span>
              </Box>
            ),
          },
          {
            accessorFn: (row) => new Date(row.addedOn), //convert to Date for sorting and filtering
            id: "addedOn",
            header: "Added On",
            enableColumnFilter: false, // could disable just this column's filter
            filterFn: "lessThanOrEqualTo",
            sortingFn: "datetime",
            size: isSmallScreen ? 240 : 1000,
            Cell: ({ cell }) => cell.getValue<Date>()?.toLocaleDateString(), //render Date as a string
            Header: ({ column }) => <em>{column.columnDef.header}</em>, //custom header markup
            //Custom Date Picker Filter from @mantine/dates
            Filter: ({ column }) => (
              <DatePickerInput
                placeholder="Filter by Added On"
                onChange={(newValue: Date) => {
                  column.setFilterValue(newValue);
                }}
                value={column.getFilterValue() as Date}
                modalProps={{ withinPortal: true }}
              />
            ),
          },
        ],
      },
      {
        id: "id",
        header: "",
        columns: [
          {
            accessorFn: (row) => `${row.id}`,
            header: "Action",
            enableColumnFilter: false, // could disable just this column's filter
            enableSorting: false, // disable sorting for this column
            Cell: ({ cell }) => (
              <Box
                sx={{
                  display: "flex",
                  alignItems: "center",
                  gap: "16px",
                }}
              >
                <button
                  className="btn btn-neutral py-0"
                  onClick={() =>
                    handleAssetDelete(Number(cell.getValue<number>()))
                  }
                  disabled={userRole.toLowerCase() === "owner"}
                >
                  Delete
                </button>
              </Box>
            ),
          },
        ],
      },
    ],
    [isSmallScreen] // Include isSmallScreen in dependency array
  );

  if (isLoading) {
    return <LoadingScreen />;
  } else
    return (
      <MantineReactTable
        columns={columns}
        data={assets.reverse()}
        enableColumnFilterModes
        enableRowNumbers
        enableFullScreenToggle={false}
        enableColumnActions={false}
        enableHiding={false}
        initialState={{
          showColumnFilters: false,
          showGlobalFilter: false,
          pagination: { pageSize: 5, pageIndex: 0 },
        }}
        positionToolbarAlertBanner="bottom"
      />
    );
};

export default AssetsTable;
